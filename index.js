const express = require('express')
const app = express()
const port = 3000
const path = require('path')

//menggunakan EJS
app.set('view engine', 'ejs');

app.get('/', (req, res) => {
    res.render('home');

});

app.get('/home', (req, res) => {
    res.render('home');

});

app.get('/work', (req, res) => {
    res.render('work'); //sudah menggunakan ejs
})

app.get('/contact', (req, res) => {
    res.render('contact');
})

app.get('/about', (req, res) => {
    res.render('about');
})

app.get('/games', (req, res) => {
    res.render('games');
})

app.use(express.static(path.join(__dirname, 'public')));
// app.use(express.static(path.join('../app')));

app.use('/', (req, res) => {
    res.status(404)
    res.send('<h1>404</h1>')
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})